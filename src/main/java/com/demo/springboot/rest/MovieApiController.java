package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.CSVFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");

        List<MovieDto> movies;
        CSVFileReader readerCSV = new CSVFileReader();
        movies = readerCSV.myRead();
        return new MovieListDto(movies);
    }
}